package com.company.shorturl.repository;

import com.company.shorturl.constants.DatabaseConstants;
import com.company.shorturl.repository.models.UrlsMongo;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@ConditionalOnProperty(value = DatabaseConstants.DATABASE, havingValue = DatabaseConstants.MONGO)
@Repository
public interface UrlsMongoRepository extends MongoRepository<UrlsMongo, Long> {

  Optional<UrlsMongo> findBySlug(String longUrl);

  Optional<UrlsMongo> findByLongUrl(String slug);

}
