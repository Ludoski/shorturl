package com.company.shorturl.repository;

import com.company.shorturl.constants.DatabaseConstants;
import com.company.shorturl.repository.models.UrlsPostgres;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@ConditionalOnProperty(value = DatabaseConstants.DATABASE, havingValue = DatabaseConstants.POSTGRES)
@Repository
public interface UrlsPostgresRepository extends CrudRepository<UrlsPostgres, Long> {

  Optional<UrlsPostgres> findBySlug(String longUrl);

  Optional<UrlsPostgres> findByLongUrl(String slug);

}
