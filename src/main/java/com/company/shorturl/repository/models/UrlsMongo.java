package com.company.shorturl.repository.models;

import com.company.shorturl.constants.DatabaseConstants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(DatabaseConstants.MONGO_URLS_COLLECTION)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UrlsMongo {

  @Transient
  public static final String SEQUENCE_NAME = DatabaseConstants.MONGO_URLS_SEQUENCE;

  @Id
  private Long id;

  private String longUrl;

  private String slug;

}
