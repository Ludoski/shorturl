package com.company.shorturl.repository.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Entity
@Table(name = "urls")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UrlsPostgres implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "urls_seq")
  @Column(name = "id")
  private Long id;

  @Column(name = "long_url")
  private String longUrl;

  @Column(name = "slug")
  private String slug;

}
