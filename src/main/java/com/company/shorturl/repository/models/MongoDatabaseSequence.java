package com.company.shorturl.repository.models;
import com.company.shorturl.constants.DatabaseConstants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(DatabaseConstants.MONGO_URLS_DATABASE_SEQUENCES)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MongoDatabaseSequence {

  @Id
  private String id;

  private long sequence;

}
