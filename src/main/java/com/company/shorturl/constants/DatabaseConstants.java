package com.company.shorturl.constants;

public class DatabaseConstants {

  public static final String DATABASE = "spring.db";

  public static final String POSTGRES = "postgres";
  public static final String MONGO = "mongodb";

  public static final String MONGO_URLS_COLLECTION = "urls";
  public static final String MONGO_URLS_DATABASE_SEQUENCES = "urls_database_sequences";
  public static final String MONGO_URLS_SEQUENCE = "urls_database_sequence";

}
