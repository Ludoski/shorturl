package com.company.shorturl.controllers.impl;

import com.company.shorturl.controllers.SaveLongUrlController;
import com.company.shorturl.dtos.url.UrlsDTO;
import com.company.shorturl.services.urls.SaveLongUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SaveLongUrlControllerImpl implements SaveLongUrlController {

  @Autowired
  private SaveLongUrlService saveLongUrlService;

  @Override
  public UrlsDTO saveLongUrl(UrlsDTO urlsDTO) {
    return saveLongUrlService.saveLongUrl(urlsDTO);
  }

}
