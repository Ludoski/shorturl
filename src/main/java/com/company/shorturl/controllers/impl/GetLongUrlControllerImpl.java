package com.company.shorturl.controllers.impl;

import com.company.shorturl.controllers.GetLongUrlController;
import com.company.shorturl.dtos.url.UrlsDTO;
import com.company.shorturl.services.urls.GetLongUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetLongUrlControllerImpl implements GetLongUrlController {

  @Autowired
  private GetLongUrlService getLongUrlService;

  @Override
  public UrlsDTO getLongUrl(String slug) {
    return getLongUrlService.getLongUrl(slug);
  }

}
