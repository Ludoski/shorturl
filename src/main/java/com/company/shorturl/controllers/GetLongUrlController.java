package com.company.shorturl.controllers;

import com.company.shorturl.dtos.url.UrlsDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

public interface GetLongUrlController {

  @GetMapping("/url/{slug}")
  @ResponseBody
  UrlsDTO getLongUrl(@PathVariable("slug") String slug);

}
