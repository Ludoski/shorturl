package com.company.shorturl.controllers;

import com.company.shorturl.dtos.url.UrlsDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

public interface SaveLongUrlController {

  @PostMapping("/url")
  @ResponseBody
  UrlsDTO saveLongUrl(@RequestBody UrlsDTO urlsDTO);

}
