package com.company.shorturl.dtos.url;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public record UrlsDTO(

        @JsonProperty
        Long id,

        @JsonProperty("long_url")
        String longUrl,

        @JsonProperty("slug")
        String slug

) {}
