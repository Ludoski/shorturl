package com.company.shorturl.services.mongo.impl;

import com.company.shorturl.repository.models.MongoDatabaseSequence;
import com.company.shorturl.services.mongo.MongoSequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class MongoSequenceGeneratorServiceImpl implements MongoSequenceGeneratorService {

  @Autowired
  private MongoOperations mongoOperations;

  @Override
  public Long generateSequence(String seqName) {

    MongoDatabaseSequence counter = mongoOperations.findAndModify(query(where("_id").is(seqName)),
            new Update().inc("sequence",1), options().returnNew(true).upsert(true),
            MongoDatabaseSequence.class);
    return !Objects.isNull(counter) ? counter.getSequence() : 0;

  }

}
