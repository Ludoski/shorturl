package com.company.shorturl.services.mongo;

public interface MongoSequenceGeneratorService {

  Long generateSequence(String seqName);

}
