package com.company.shorturl.services.urls;

public interface UrlOperationsService {

  String idToSlug(Long id);

  Long slugToId(String slug);

}
