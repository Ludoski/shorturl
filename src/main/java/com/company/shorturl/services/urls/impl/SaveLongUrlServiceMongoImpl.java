package com.company.shorturl.services.urls.impl;

import com.company.shorturl.constants.CacheConstants;
import com.company.shorturl.constants.DatabaseConstants;
import com.company.shorturl.dtos.url.UrlsDTO;
import com.company.shorturl.repository.UrlsMongoRepository;
import com.company.shorturl.repository.models.UrlsMongo;
import com.company.shorturl.services.mongo.MongoSequenceGeneratorService;
import com.company.shorturl.services.urls.SaveLongUrlService;
import com.company.shorturl.services.urls.UrlOperationsService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

import java.util.Optional;

@ConditionalOnProperty(value = DatabaseConstants.DATABASE, havingValue = DatabaseConstants.MONGO)
@CacheConfig(cacheNames={CacheConstants.URLS})
@Service
public class SaveLongUrlServiceMongoImpl implements SaveLongUrlService {

  @Autowired
  private MongoSequenceGeneratorService sequenceGeneratorService;

  @Autowired
  private UrlsMongoRepository urlsRepository;

  @Autowired
  private UrlOperationsService urlOperationsService;

  @Override
  @Transactional
  @CachePut(key = "#result.slug")
  public UrlsDTO saveLongUrl(UrlsDTO urlsDTO) {
    Optional<UrlsMongo> optionalUrls = urlsRepository.findByLongUrl(urlsDTO.longUrl());

    if (optionalUrls.isPresent()) {
      UrlsMongo urls = optionalUrls.get();
      return new UrlsDTO(urls.getId(), urls.getLongUrl(), urls.getSlug());
    }

    UrlsMongo urls = new UrlsMongo();
    urls.setId(sequenceGeneratorService.generateSequence(UrlsMongo.SEQUENCE_NAME));
    urls.setLongUrl(urlsDTO.longUrl());
    urls.setSlug("N/A");

    urlsRepository.save(urls);

    String slug = urlOperationsService.idToSlug(urls.getId());
    urls.setSlug(slug);

    urlsRepository.save(urls);

    return new UrlsDTO(urls.getId(), urls.getLongUrl(), urls.getSlug());
  }

}
