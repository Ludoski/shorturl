package com.company.shorturl.services.urls.impl;

import com.company.shorturl.services.urls.UrlOperationsService;
import org.springframework.stereotype.Service;

@Service
public class UrlOperationsServiceImpl implements UrlOperationsService {

  // All letters and digits
  //private static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  // No vowels a, e, i, o, u
  // No unambiguous characters I, l, 1, 0, O
  // Added symbols $, -, _, ., +, !, *, ', (, )
  private static final String CHARACTERS = "bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ23456789$-_.+!*'()";

  private static final int CHARACTERS_LENGTH = CHARACTERS.length();

  public String idToSlug(Long id) {
    StringBuilder sb = new StringBuilder();

    while (id > 0) {
      sb.append(CHARACTERS.charAt(Math.toIntExact(id % CHARACTERS_LENGTH)));
      id /= CHARACTERS_LENGTH;
    }

    return sb.reverse().toString();
  }

  public Long slugToId(String slug) {
    long id = 0L;

    for (int i = 0; i < slug.length(); i++) {
      id = id * CHARACTERS_LENGTH + CHARACTERS.indexOf(slug.charAt(i));
    }

    return id;
  }

}
