package com.company.shorturl.services.urls.impl;

import com.company.shorturl.constants.CacheConstants;
import com.company.shorturl.constants.DatabaseConstants;
import com.company.shorturl.dtos.url.UrlsDTO;
import com.company.shorturl.exceptions.UrlNotFoundException;
import com.company.shorturl.repository.UrlsPostgresRepository;
import com.company.shorturl.repository.models.UrlsPostgres;
import com.company.shorturl.services.urls.GetLongUrlService;
import com.company.shorturl.services.urls.UrlOperationsService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@ConditionalOnProperty(value = DatabaseConstants.DATABASE, havingValue = DatabaseConstants.POSTGRES)
@CacheConfig(cacheNames={CacheConstants.URLS})
@Service
public class GetLongUrlServicePostgresImpl implements GetLongUrlService {

  @Autowired
  private UrlsPostgresRepository urlsRepository;

  @Autowired
  private UrlOperationsService urlOperationsService;

  @Override
  @Transactional
  @Cacheable
  public UrlsDTO getLongUrl(String slug) {
    Optional<UrlsPostgres> optionalUrls = urlsRepository.findBySlug(slug);

    if (optionalUrls.isEmpty()) {
      throw new UrlNotFoundException();
    }

    UrlsPostgres urls = optionalUrls.get();
    return new UrlsDTO(urls.getId(), urls.getLongUrl(), urls.getSlug());
  }

}
