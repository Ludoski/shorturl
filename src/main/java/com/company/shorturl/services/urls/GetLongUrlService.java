package com.company.shorturl.services.urls;

import com.company.shorturl.dtos.url.UrlsDTO;

public interface GetLongUrlService {

  UrlsDTO getLongUrl(String slug);

}
