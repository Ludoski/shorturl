package com.company.shorturl.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Url not found exception
 */
public class UrlNotFoundException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "Url not found.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.NOT_FOUND;

  public UrlNotFoundException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
